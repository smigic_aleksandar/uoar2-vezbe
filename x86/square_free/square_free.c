#include <stdio.h>
#include <stdlib.h>

int square_free(int n);

int main() {
	int n;
	scanf("%d", &n);
	printf("%d\n", square_free(n));
	return 0;
}
