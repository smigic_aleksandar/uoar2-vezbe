#include <stdio.h>
#include <stdlib.h>

typedef unsigned int uint;

uint reverse(uint n);

uint reverse_impl(uint n) {
	uint res = 0;
	while (n != 0) {
		res *= 10;
		res += n % 10;
		n /= 10;
	}
	return res;
}

int main() {
	uint n;
	scanf("%u", &n);
	printf("%u\n", reverse(n));
	return 0;
}
