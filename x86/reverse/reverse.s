.intel_syntax noprefix

.text
.global reverse

# edi
#  n
reverse:
	enter 0, 0

	mov r8d, 0   # sum = 0
	mov r10d, 10 # 10
	start_loop:
		cmp edi, 0
		je end_loop
		
		# sum *= 10
		mov eax, r8d
		mul r10d
		mov r8d, eax

		# r9d = n % 10
		mov eax, edi
		cdq
		div r10d
		mov r9d, edx 

		add r8d, r9d # sum += n % 10

		# n /= 10
		mov eax, edi
		cdq
		div r10d
		mov edi, eax
		
		jmp start_loop
	end_loop:

	mov eax, r8d

	leave
	ret
