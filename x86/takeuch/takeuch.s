.intel_syntax noprefix

.text

.global takeuch

# edi, esi, edx
takeuch:
	enter 0, 0

	cmp edi, esi # if (x <= y) return z
	jg recur
	mov eax, edx
	jmp end

recur:
	push rdi
	push rsi
	push rdx

	# f(x - 1, y, z)
	dec edi
	call takeuch

	pop rdx
	pop rsi
	pop rdi

	push rax

	push rdi
	push rsi
	push rdx

	# f(y - 1, z, x)
	dec esi
	mov r8d, edi
	mov r9d, esi
	mov r10d, edx

	mov edi, r9d
	mov esi, r10d
	mov edx, r8d

	call takeuch

	pop rdx
	pop rsi
	pop rdi

	push rax
	
	push rdi
	push rsi
	push rdx

	# f(z - 1, x, y)
	dec edx
	mov r8d, edi
	mov r9d, esi
	mov r10d, edx

	mov edi, r10d
	mov esi, r8d
	mov edx, r9d 

	call takeuch

	pop rdx
	pop rsi
	pop rdi

	push rax
	
	pop rdx
	pop rsi
	pop rdi

	call takeuch
	inc eax
end:
	leave
	ret
