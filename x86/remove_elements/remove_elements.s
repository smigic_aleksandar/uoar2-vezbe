.intel_syntax noprefix

.text
.global remove_elements
# rdi esi
# a[]  n
remove_elements:
	enter 0, 0

	mov r8d, 0       # sum = 0
	mov r9d, esi
	dec r9d          # i = n - 1

	start_sum_loop:
		cmp r9d, 0
		jl end_sum_loop

		add r8d, [rdi + 4 * r9] 

		dec r9d
		jmp start_sum_loop
	end_sum_loop:

	mov r9d, esi
	dec r9d      # i = n - 1
	mov eax, r9d
	start_mark_loop:
		cmp r9d, 0
		jl end_mark_loop
		
		mov r10d, [rdi + 4 * r9] # cur = a[i]
		sub r8d, r10d # sum -= a[i]

		# if (sum % a[i] != 0) a[i] = 0
		mov eax, r8d
		cdq
		div r10d
		cmp edx, 0
		je continue_mark

		mov dword ptr [rdi + 4 * r9], 0

	continue_mark:
		dec r9d
		jmp start_mark_loop
	end_mark_loop:

	mov r8d, 0 # next = 0	
	mov r9d, 0 # i = 0
	start_dense_loop:
		cmp r9d, esi
		je end_dense_loop
	
		mov r10d, [rdi + 4 * r9] # cur = a[i]
		cmp r10d, 0
		je continue_dense

		# a[next++] = a[i]
		mov [rdi + 4 * r8], r10d
		inc r8d
		
	continue_dense:	
		inc r9d
		jmp start_dense_loop
	end_dense_loop:
	

	mov eax, r8d

	leave
	ret
