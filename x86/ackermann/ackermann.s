.intel_syntax noprefix
.text

.global ackermann
# edi esi
#  m   n
ackermann:
	enter 0, 0 

	cmp edi, 0
	jne recur
	
	# return n + 1
	inc esi
	mov eax, esi
	jmp done

recur:
	cmp esi, 0
	je one
	jne two

# A(m - 1, 1)
one:
	push rdi
	push rsi
	dec edi
	mov esi, 1
	call ackermann
	pop rsi
	pop rdi
	jmp done

two:
	# A(m, n - 1)
	push rdi
	push rsi
	dec esi
	call ackermann
	pop rsi
	pop rdi
	
	# A(m - 1, A(m, n - 1))
	push rdi
	push rsi
	dec edi
	mov esi, eax
	call ackermann
	pop rsi
	pop rdi
	jmp done

done:
	leave
	ret
