#include <stdio.h>
#include <stdlib.h>

int ackermann(int m, int n);

int main() {
	int m, n;
	scanf("%d %d", &m, &n);
	printf("%d\n", ackermann(m, n));
	return 0;
}
