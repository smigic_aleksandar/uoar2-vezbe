.intel_syntax noprefix

.text

# edi esi
#  a   b
.global count_primes
count_primes:
	enter 0, 0

	mov eax, 0 # count = 0

	start_loop:
		cmp edi, esi
		jg end_loop

		push rax
		push rdi
		push rsi

		# edi je vec postavljen
		call is_prime
		mov r8d, eax

		pop rsi
		pop rdi
		pop rax

		cmp r8d, 1
		jne continue

		inc eax	
	
	continue:

		inc edi
		jmp start_loop
	end_loop:

	leave
	ret


# edi
#  n
is_prime:
	enter 0, 0

	# n = edi / 2
	mov r8d, edi
	shr r8d

	start_prime_loop:
		cmp r8d, 1
		je yes_prime

		# if n % i == 0
		cdq
		mov eax, edi
		div r8d

		cmp edx, 0
		je not_prime
		
		dec r8d
		jmp start_prime_loop
	end_prime_loop:

yes_prime:
	mov eax, 1
	jmp end

not_prime:
	mov eax, 0
	jmp end
	
end:
	leave
	ret
