#include <stdio.h>
#include <stdlib.h>

int count_primes(int a, int b);

int is_prime_impl(int n) {
	if (n <= 1) { return 0; }
	for (int i = 2; i * i <= n; i++) {
		if (n % i == 0) {
			return 0;
		}
	}
	return 1;
}

int main() {
	int a, b;
	scanf("%d %d", &a, &b);
	printf("%d\n", count_primes(a, b));
	return 0;
}
