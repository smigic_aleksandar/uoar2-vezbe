.intel_syntax noprefix
.text

# edi = n
.global secret
secret:
	enter 0, 0

	start_loop: # while n >= 10
		cmp edi, 10	
		jl end_loop

		mov r9d, 1 # exp = pow(10, 0) = 1
		mov ecx, 0 # res = 0

		start_inner_loop: # while n >= 10
			cmp edi, 10	
			jl end_inner_loop

			# r10d = n % 100
			mov eax, edi
			mov esi, 100
			cdq
			div esi
			mov r10d, edx

			# r10d = 'ab'
			# eax = a
			# edx = b
			mov eax, r10d
			mov esi, 10
			cdq
			div esi

			# a = a + b
			add eax, edx

			cmp eax, 10 # if a >= 10
			jl continue

			# b = a % 10
			# a = a / 10
			# a += b
			mov esi, 10
			cdq
			div esi
			add eax, edx

		continue:
			# u eax je vec suma tj. a
			mul r9d # a * exp
			add ecx, eax # res += a * exp

			# exp *= 10
			mov eax, r9d 
			mov esi, 10
			mul esi
			mov r9d, eax

			# n /= 10
			mov eax, edi
			mov esi, 10
			div esi
			mov edi, eax
			
			jmp start_inner_loop
		end_inner_loop:

		mov edi, ecx
		jmp start_loop
	end_loop:

	mov eax, edi

	leave
	ret
