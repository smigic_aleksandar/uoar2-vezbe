#include <stdio.h>
#include <stdlib.h>

int remove_elements(int* a, int n);

int remove_elements_impl(int *a, int n) {
	int sum = 0;
	for (int i = 0; i < n; i++) {
		sum += a[i];
	}
	for (int i = n - 1; i >= 0; i--) {
		sum -= a[i];
		if (a[i] == 0 || sum % a[i] != 0) {
			a[i] = 0;
		}
	}
	int next = 0;
	for (int i = 0; i < n; i++) {
		if (a[i] != 0) {
			a[next++] = a[i];
		}
	}

	return next;
}

int main() {
	/*
	int n;
	scanf("%d", &n);
	*/
	int a[] = {3, 4, 1, 9, 8, 5};
	int n = sizeof(a) / sizeof(int);
	n = remove_elements_impl(a, n);
	printf("n: %d\n", n);
	for (int i = 0; i < n; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");
	return 0;
}
