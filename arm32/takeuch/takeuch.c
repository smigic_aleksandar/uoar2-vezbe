#include <stdio.h>
#include <stdlib.h>
#define f takeuch_impl 

int takeuch(int x, int y, int z);

int takeuch_impl(int x, int y, int z) {
	return x > y ? f(f(x - 1, y, z), f(y - 1, z, x), f(z - 1, x, y)) + 1 : z;
}

int main() {
	int x, y, z;
	scanf("%d %d %d", &x, &y, &z);
	printf("%d\n", takeuch(x, y, z));
	return 0;
}
