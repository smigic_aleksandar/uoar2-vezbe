.text
.align 2

.global takeuch

# r0 r1 r2
# x  y  z
takeuch:
	stmfd sp!, {fp, lr}
	mov fp, sp

	cmp r0, r1
	ble recur

	mov r0, r2
	b done

recur:
	stmfd sp!, {r4-r9}

	; t(x - 1, y, z)
	stmfd sp!, {r0-r2}
	sub r0, r0, #1
	bl takeuch 
	mov r7, r0 ; k1
	ldmfd sp!, {r4-r6}
	stmfd sp!, {r4-r6}

	; t(y - 1, z, x)
	sub r5, r5, #1
	mov r0, r5
	mov r1, r6
	mov r2, r4
	bl takeuch
	mov r8, r0 ; k2
	ldmfd sp!, {r4-r6}

	; t(z - 1, x, y)
	sub r6, r6, #1
	mov r0, r6
	mov r1, r4
	mov r2, r5
	bl takeuch
	mov r9, r0 ; k3

	; t(k1, k2, k3)
	mov r0, r7
	mov r1, r8
	mov r2, r9
	bl takeuch
	add r0, r0, #1

	ldmfd sp!, {r4-r9}

done:
	mov sp, fp
	ldmfd sp!, {fp, pc}
