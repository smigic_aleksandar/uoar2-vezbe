#include <stdio.h>
#include <stdlib.h>

int secret(int n);

int secret_impl(int n) {
	while (n >= 10) {
		int res = 0;
		int exp = 1;
		while (n >= 10) {
			int ab = n % 100;
			int b = ab % 10;
			int a = ab / 10;
			a += b;
			if (a >= 10) {
				b = a % 10;
				a = a / 10;
				a += b;
			}
			res += a * exp;
			exp *= 10;
			n /= 10;
		}
		n = res;
	}
	return n;
}

int main() {
	int n;
	scanf("%d", &n);
	printf("%d\n", secret(n));
	return 0;
}

