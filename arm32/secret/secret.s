.text
.align 2

.global secret
secret:
	stmfd sp!, {fp, lr}
	mov fp, sp

	stmfd sp!, {r4-r9}

	mov r4, r0 ; n
	start_loop:
		cmp r4, #10
		bge end_loop

		mov r8, #0 ; res = 0
		mov r9, #1 ; exp = 1

		start_inner_loop:
			cmp r4, #10
			bge end_inner_loop

			; r5 = n % 100
			mov r0, r4	
			mov r1, #100
			bl __modsi3
			mov r5, r0 
			
			; b = r5 % 10
			mov r0, r5
			mov r1, #10
			bl __modsi3
			mov r7, r0 

			; a = r5 / 10
			mov r0, r5
			mov r1, #10
			bl __divsi3
			mov r6, r0

			add r6, r6, r7 ; a += b

			cmp r6, #10
			bl continue_inner

			; b = r6 % 10		
			mov r0, r6
			mov r1, #10
			bl __divsi3
			mov r7, r0

			; a = r6 / 10		
			mov r0, r6
			mov r1, #10
			bl __divsi3
			mov r6, r0

			; a += b
			add r6, r6, r7
		
			; r6 *= exp
			mul r6, r6, r9
			; exp *= 10
			mul r9, r9, #10
			; res += r6
			add r8, r8, r6	
			; n /= 10
			mov r0, r4
			mov r1, #10
			bl __divsi3
			mov r4, r0

			b start_inner_loop
		end_inner_loop:
		; n = res
		mov r4, r8		
		b start_loop
	end_loop:

	mov r0, r4

	ldmfd sp!, {r4-r9}

	mov sp, fp
	ldmfd sp!, {fp, pc}
