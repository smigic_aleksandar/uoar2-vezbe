.intel_syntax noprefix

.text
.global square_free

# edi
#  n
square_free:
	enter 0, 0
	
	mov ecx, edi
	shr ecx # ecx = n / 2

	mov r10d, 0 # broj prostih fakora koji dele n

	start_loop:
		cmp ecx, 1
		je end_loop

		# if (n % i == 0) 
		mov eax, edi
		cdq
		div ecx
		cmp edx, 0
		jne continue
		
		inc r10d

		# r8d = ecx^2
		mov eax, ecx
		mul ecx
		mov r8d, eax

		# if (n % i^2 != 0)
		mov eax, edi
		cdq
		div r8d
		cmp edx, 0
		je not_sqf

	continue:
		dec ecx
		jmp start_loop
	end_loop:

	cmp r10d, 0 # ako je 0 prostih faktora koji dele n onda je n prosto
	je not_sqf

yes_sqf:
	mov eax, 1
	jmp done

not_sqf:
	mov eax, 0
	jmp done

done:
	leave
	ret

