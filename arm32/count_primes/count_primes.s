.text
.align 2

.global count_primes

; r0 r1
; a  b
count_primes:
	stmfd sp!, {fp, lr}
	mov fp, sp
		
	stmfd sp!, {r4-r6}

	mov r4, r0 ; a
	mov r5, r1 ; b
	mov r6, #0 ; count = 0
	start_loop:
		cmp r4, r5
		bgt end_loop

		# if is_prime(i): count++
		mov r0, r4
		bl is_prime
		cmp r0, #1
		addeq r6, r6, #1
		
		add r4, r4, #1	
	end_loop:

	ldmfd sp!, {r4-r6}

	mov sp, fp
	ldmfd sp!, {fp, lr}

# n
# r0
is_prime:
	stmfd sp!, {fp, lr}
	mov fp, sp

	stmfd sp!, {r4}
	
	mov r4, r0, lsr #1

	start_prime_loop:
		cmp r4, #1
		beq end_prime_loop

		# if n % 10 != 0
		mov r0, r4
		mov r1, #10
		bl __modsi3

		cmp r0, #0
		moveq r0, #0
		beq done_prime

		sub r4, r4, #1
		b start_prime_loop
	end_prime_loop:
	mov r0, #1

done_prime:
	ldmfd sp!, {r4}

	mov sp, fp
	ldmfd sp!, {fp, lr}
